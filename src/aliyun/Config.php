<?php
// +----------------------------------------------------------------------
// | 阿里云短信
// +----------------------------------------------------------------------
// | 日期 2024/03/22 18:23 下午
// +----------------------------------------------------------------------
// | 开发者 Even <yinxu46@qq.com>
// +----------------------------------------------------------------------
// | 版权所有 2023~2024 苏州千朵网络科技有限公司 [ https://www.1000duo.cn ]
// +----------------------------------------------------------------------

namespace frappe\sms\aliyun;

/**
 * 配置
 */
class Config
{
    public $accessKeyId = "";
    public $accessKeySecret = "";
    public $signName = "";
    public $domain = "";
    public $regionId = "";
    public $version = "";
    public $security = false;
    public $template = [];
}