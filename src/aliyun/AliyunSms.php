<?php
// +----------------------------------------------------------------------
// | 阿里云短信
// +----------------------------------------------------------------------
// | 日期 2024/03/22 18:23 下午
// +----------------------------------------------------------------------
// | 开发者 Even <yinxu46@qq.com>
// +----------------------------------------------------------------------
// | 版权所有 2023~2024 苏州千朵网络科技有限公司 [ https://www.1000duo.cn ]
// +----------------------------------------------------------------------

namespace frappe\sms\aliyun;

use Exception;
use InvalidArgumentException;
use frappe\sms\exceptions\SmsException;

/**
 * 阿里云短信客户端
 */
class AliyunSms
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * Sms constructor.
     * @param array $options
     */
    function __construct(array $options)
    {
        if (empty($options['accessKeyId'])) {
            throw new InvalidArgumentException("Missing Aliyun Sms Config -- [accessKeyId]");
        }
        if (empty($options['accessKeySecret'])) {
            throw new InvalidArgumentException("Missing Aliyun Sms Config -- [accessKeySecret]");
        }
        if (empty($options['signName'])) {
            throw new InvalidArgumentException("Missing Aliyun Sms Config -- [signName]");
        }
        if (empty($options['domain'])) {
            throw new InvalidArgumentException("Missing Aliyun Sms Config -- [domain]");
        }
        if (empty($options['regionId'])) {
            throw new InvalidArgumentException("Missing Aliyun Sms Config -- [regionId]");
        }
        if (empty($options['version'])) {
            throw new InvalidArgumentException("Missing Aliyun Sms Config -- [version]");
        }
        $this->config = new Config();
        $this->config->accessKeyId = $options['accessKeyId'];
        $this->config->accessKeySecret = $options['accessKeySecret'];
        $this->config->signName = $options['signName'];
        $this->config->domain = $options['domain'];
        $this->config->regionId = $options['regionId'];
        $this->config->version = $options['version'];
        $this->config->template = $options['template'] ?? [];
    }

    /**
     * 发送短信
     * @param string|array $phoneNumbers 手机号
     * @param string $template 短信模版：login、register、forgot
     * @param array $templateParam 模版参数：["code" => 1234]
     * @param string $outId
     * @param string $smsUpExtendCode
     * @return bool
     * @throws SmsException
     * @author yinxu
     * @date 2024/3/23 00:07:14
     */
    public function send($phoneNumbers, string $template, array $templateParam = [], string $outId = '', string $smsUpExtendCode = ''): bool
    {
        if (empty($phoneNumbers)) {
            throw new InvalidArgumentException("Missing Aliyun Sms Config -- [PhoneNumbers]");
        }
        if (empty($template) || !isset($this->config->template[$template])) {
            throw new InvalidArgumentException("Missing Aliyun Sms Config Template -- [$template]");
        }
        if ($templateParam) $templateParam = json_encode($templateParam, JSON_UNESCAPED_UNICODE);

        $data = [
            'PhoneNumbers' => $phoneNumbers,
            'TemplateCode' => $this->config->template[$template],
            'SignName' => $this->config->signName,
        ];

        if ($templateParam) $data['TemplateParam'] = $templateParam;
        if ($outId) $data['OutId'] = $outId;
        if ($smsUpExtendCode) $data['SmsUpExtendCode'] = $smsUpExtendCode;

        $res = $this->request('SendSms', $data);

        if (empty($res) || empty($res['Code'])) {
            throw new SmsException("未知异常");
        }

        if ($res['Code'] != 'OK') {
            throw new SmsException(ErrorCode::$message[$res['Code']] ?? '未知异常');
        }

        return true;
    }

    /**
     * 接口请求
     * @param string $action
     * @param array $data
     * @return mixed
     * @throws SmsException
     * @author yinxu
     * @date 2024/3/23 00:05:37
     */
    private function request(string $action, array $data = [])
    {
        $apiParams = array_merge(array(
            "SignatureMethod" => "HMAC-SHA1",
            "SignatureNonce" => uniqid(mt_rand(0, 0xffff), true),
            "SignatureVersion" => "1.0",
            "Timestamp" => gmdate("Y-m-d\TH:i:s\Z"),
            "Format" => "JSON",
            "Version" => $this->config->version,
            "AccessKeyId" => $this->config->accessKeyId,
            "Action" => $action,
        ), $data);
        ksort($apiParams);
        $sortedQueryStringTmp = "";
        foreach ($apiParams as $key => $value) {
            $sortedQueryStringTmp .= "&" . $this->encode($key) . "=" . $this->encode($value);
        }

        $stringToSign = "GET&%2F&" . $this->encode(substr($sortedQueryStringTmp, 1));

        $sign = base64_encode(hash_hmac("sha1", $stringToSign, $this->config->accessKeySecret . "&", true));

        $signature = $this->encode($sign);

        $url = ($this->config->security ? 'https' : 'http') . "://{$this->config->domain}/?Signature=$signature$sortedQueryStringTmp";

        return $this->doRequest($url);
    }

    /**
     * 发送请求
     * @param string $url
     * @return mixed
     * @throws SmsException
     * @author yinxu
     * @date 2024/3/23 00:05:18
     */
    private function doRequest(string $url)
    {
        try {
            $content = $this->fetchContent($url);
            return json_decode($content, true);
        } catch (Exception $e) {
            throw new SmsException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * 编码
     * @param string $str
     * @return string
     * @author yinxu
     * @date 2024/3/22 18:33:46
     */
    private function encode(string $str): string
    {
        if (empty($str)) return $str;
        $res = urlencode($str);
        $res = preg_replace("/\+/", "%20", $res);
        $res = preg_replace("/\*/", "%2A", $res);
        return (string)preg_replace("/%7E/", "~", $res);
    }

    /**
     * curl 请求
     * @param string $url
     * @return bool|mixed|string
     * @author yinxu
     * @date 2024/3/22 18:33:57
     */
    private function fetchContent(string $url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "x-sdk-client" => "php/2.0.0"
        ));

        if (substr($url, 0, 5) == 'https') {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        }

        $rtn = curl_exec($ch);

        if ($rtn === false) {
            trigger_error("[CURL_" . curl_errno($ch) . "]: " . curl_error($ch), E_USER_ERROR);
        }

        curl_close($ch);

        return $rtn;
    }
}