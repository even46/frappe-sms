<?php
declare(strict_types=1);

use frappe\sms\exceptions\SmsException;
use frappe\sms\facade\Sms;
use think\helper\Str;

if (!function_exists('frappe_sms_send')) {
    /**
     * 发送短信
     * @param string|int|string[]|int[] $phones 手机号
     * @param string $template 短信模版
     * @param array $params 短信参数
     * @param string|null $config 短信服务商名称
     * @param array $extra 扩展参数
     * @return bool
     * @throws SmsException
     * @author yinxu
     * @date 2024/3/22 19:36:31
     */
    function frappe_sms_send($phones, string $template, array $params = [], string $config = null, array $extra = []): bool
    {
        return Sms::send($phones, $template, $params, $config, $extra);
    }
}

if (!function_exists('frappe_sms_code_send')) {
    /**
     * 发送短信验证码
     * @param string|int $phone 手机号
     * @param string $template 短信模版
     * @param string $client 客户端
     * @param int $expire 验证码过期时间
     * @return bool
     * @throws SmsException
     * @author yinxu
     * @date 2024/3/28 10:26:12
     */
    function frappe_sms_code_send($phone, string $template, string $client = "", int $expire = 300): bool
    {
        # 同一场景下短信验证码在1天内仅能发送10次。
        $limit_day = config('sms.limit.day', 10);
        $limit_day_key = "limit@" . date("Ymd") . "@$template@$phone";
        $has_count = intval(cache($limit_day_key) ?? 0);
        if ($has_count >= $limit_day) {
            throw new InvalidArgumentException("短信发送频繁，请于明日再试");
        }
        # 同一场景下短信验证码发送1次间隔默认60s。
        $limit_second = config('sms.limit.second', 60);
        $limit_second_key = "limit@second@$template@$phone";
        if (cache($limit_second_key) == 1) {
            throw new InvalidArgumentException("短信发送频繁，请于{$limit_second}s后再试");
        }
        # 短信验证码
        $phone_code = Str::random(6, 1);
        $params = ['code' => $phone_code];
        $result = Sms::send($phone, $template, $params, null, []);
        if ($result == true) {
            cache("$client@$template@$phone", $phone_code, $expire);
            cache($limit_second_key, 1, $limit_second);
        }
        return $result;
    }
}

if (!function_exists('frappe_sms_code_validate')) {
    /**
     * 短信验证码验证
     * @param string|int $phone 手机号
     * @param string $phone_code 短信验证码
     * @param string $template 短信模版
     * @param string $client 客户端
     * @param bool $exception 是否抛出异常，默认是
     * @return bool
     * @author yinxu
     * @date 2024/3/28 10:31:42
     */
    function frappe_sms_code_validate($phone, string $phone_code, string $template, string $client = "", bool $exception = true): bool
    {
        if (empty($phone_code)) {
            $result = false;
        } else {
            $result = (cache("$client@$template@$phone") ?? "") == $phone_code;
        }
        if (!$result && $exception) {
            throw new InvalidArgumentException("无效的短信验证码");
        }
        if ($result) {
            cache("$client@$template@$phone", null);
        }
        return $result;
    }
}