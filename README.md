
# frappe-sms

thinkphp6｜thinkphp8 短信类库

## 安装
> composer require yinxu46/frappe-sms



## 使用

### 助手函数

发送短信
~~~
public function send($mobile = '', $params = [])
{
    return frappe_sms_send($phones, string $template, array $params = [], string $config = null, array $extra = []);
}
~~~

发送短信验证码
~~~
public function send_code($mobile = '')
{
    return frappe_sms_code_send($phone, string $template, string $client = "", int $expire = 300);
}
~~~

验证短信验证码
~~~
public function validate_code($mobile = '')
{
    return frappe_sms_code_validate($phone, string $phone_code, string $template, string $client = "", bool $exception = true);
}
~~~